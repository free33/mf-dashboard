<?php
class Cl_DBclass
{
	public $con;

	public function __construct()
	{
		$this->con = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);
		mysqli_set_charset ($this->con, 'utf8');
		if( mysqli_connect_error()) echo "Failed to connect to MySQL: " . mysqli_connect_error();
	}
}

class Cl_Game
{
	// Для соединения с БД 
	protected $_con;
	
	// Инициализация  DBclass
	public function __construct()	{
		$db = new Cl_DBclass();
		$this->_con = $db->con;
	}
	
	public function getOptions(){
		$result = mysqli_query( $this->_con, "select name,value from options");	
		$options = array();
		while ( $row = mysqli_fetch_assoc($result) ) {
			$options[$row['name']] = $row['value'];
		}		
	return $options;
	}
	
	public function restartGame(){ 
		$query = "UPDATE options SET value = 0 WHERE name in ('game_start', 'game_end');";
		$result = mysqli_query($this->_con, $query);
		$query = "TRUNCATE scores";
		$result = mysqli_query($this->_con, $query);
	return $result;
	}
	
	public function startGame(){
		$query = "UPDATE options SET value = 1 WHERE name = 'game_start'";
		$result = mysqli_query($this->_con, $query);
	return $result;
	}

	public function getQuestions(){
		$this->restartGame();
		$settings =$this->getOptions();
		$result = mysqli_query( $this->_con, "select * FROM questions ORDER BY RAND() LIMIT ".$settings['num_questions']);	
		$results = array();
		$serverquestions = array();
		$results['rowcount'] =  mysqli_num_rows( $result );
		while ( $row = mysqli_fetch_assoc($result) ) {
			$serverquestions[] = $row['id'];
			$results['questions'][] = $row;
		}
		$q = implode(", ", $serverquestions);
		mysqli_query( $this->_con, "UPDATE options SET value = '$q' WHERE name = 'questions' ");	
			
	return $results;
	}

	public function endGame(){
		$query = "UPDATE options SET VALUE = 1 WHERE name = 'game_end'";
		$result = mysqli_query($this->_con, $query);	
	return $result;
	}	
	
	public function getWinners()	{
		$query = "SELECT * FROM (SELECT scores.id, user_id, score, phone FROM scores LEFT JOIN users ON scores.user_id = users.id WHERE score != 0 ORDER BY user_id, score ASC ) AS x GROUP BY user_id ORDER BY x.score ASC LIMIT 5";
		$result = mysqli_query($this->_con, $query);
		$i=0;
		$pics = array();
		foreach (glob("img/emoji/*.png") as $filename) {
		$pics[] = $filename;
		}
		shuffle ($pics);
		while ($row = mysqli_fetch_assoc($result)) {
			$leaders[$i]['picture'] = $pics[$i];
			$phone = $row['phone'];
			$leaders[$i]['phone'] = '+7 '. substr($phone, 1, 3).'-xxx-xx-<span class="big">'.substr($phone, -2).'</span>';
			$i++;
		}
		mysqli_close($this->_con);
	return $leaders;
	}
}

class Cl_Admin
{
	protected $_con;
	
	public function __construct()	{
		$db2 = new Cl_DBclass();
		$this->_con = $db2->con;
	}
	
	public function getOptions(){
		$result = mysqli_query( $this->_con, "select name,value from options");	
		$options = array();
		while ( $row = mysqli_fetch_assoc($result) ) {
			$options[$row['name']] = $row['value'];
		}		
	return $options;
	}
	
	public function getWinners()	{
		$query = "SELECT * FROM (SELECT scores.id, user_id, score, phone FROM scores LEFT JOIN users ON scores.user_id = users.id WHERE score != 0 ORDER BY user_id, score ASC ) AS x GROUP BY user_id ORDER BY x.score ASC LIMIT 5";
		$result = mysqli_query($this->_con, $query);
		$i=0;		
		while ($row = mysqli_fetch_assoc($result)) {
			$leaders[$i] = $row;
			$i++;
		}
	return $leaders;
	}
	
	public function getQuestions()	{
		$query = "SELECT * FROM questions";
		$result = mysqli_query($this->_con, $query);
		$i=0;
		while ($row = mysqli_fetch_assoc($result)) {
			$leaders[$i] = $row;
			$i++;
		}
	return $leaders;
	}
	
	public function updateQuestions($id,$question_name,$answer1,$answer2,$answer3,$answer4,$answer)	{
		$query = "UPDATE questions SET question_name = '$question_name', answer1 = '$answer1', answer2 = '$answer2', answer3 = '$answer3', answer4 = '$answer4', answer = '$answer' WHERE questions.id = $id";
		$result = mysqli_query($this->_con, $query);
		return $result;
	}
	public function addQuestions($id = null,$question_name = null,$answer1 = null,$answer2 = null,$answer3 = null,$answer4 = null,$answer = null)	{
		$query = "INSERT INTO questions (id,question_name,answer1,answer2,answer3,answer4,answer) VALUES ('$id','$question_name', '$answer1', '$answer2', '$answer3', '$answer4', '$answer')";
		mysqli_query($this->_con, $query);
		$result = mysqli_insert_id($this->_con); 
		return $result;
	}
	public function deleteQuestions($id){
		$query = "DELETE FROM questions WHERE id = $id";
		$result = mysqli_query($this->_con, $query);
		return $result;
	}
	public function updateOptions($time_login, $time_question_dashboard, $time_question_phone, $num_questions){
		$query = "UPDATE options SET value = CASE WHEN name = 'time_login' THEN '$time_login' WHEN name = 'time_question_dashboard' THEN '$time_question_dashboard' WHEN name = 'time_question_phone' THEN '$time_question_phone' WHEN name = 'num_questions' THEN '$num_questions' END WHERE name IN ('time_login','time_question_dashboard','time_question_phone','num_questions')";
		$result = mysqli_query($this->_con, $query);
		return $result;
	}
}