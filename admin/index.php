<?php 
ob_start();
session_start();
require_once '../config.php'; 

$user_obj = new Cl_Admin();
$settings =$user_obj->getOptions();
$questions =$user_obj->getQuestions();
$questions =$user_obj->getQuestions();
$winners =$user_obj->getWinners();
?> 
<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<title>МегаФон Квиз</title>
	<link href="bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
	<style>
.bg-mfgreen, .btn-mfgreen{background-color: #00b856; }
.bg-mfpurple, .btn-mfpurple{background-color: #731982; border-color: #731982!important;}
.btn-mfgreen:hover {color: white; border-color: #33c778important; background-color: #33c778;}
.btn-mfpurple:hover {color: white; border-color: #b808d4!important; background-color: #b808d4;}
.color-mfgreen{color: #00b856;}
.color-mfpurple{color: #731982;}

html,
body  {
  height: 100%;
}

body.login {
  display: -ms-flexbox;
  display: flex;
  -ms-flex-align: center;
  align-items: center;
  padding-top: 40px;
  padding-bottom: 40px;
}

.form-signin {
  width: 100%;
  max-width: 330px;
  padding: 15px;
  margin: auto;
}
.form-signin .checkbox {
  font-weight: 400;
}
.form-signin .form-control {
  position: relative;
  box-sizing: border-box;
  height: auto;
  padding: 10px;
  font-size: 16px;
}
.form-signin .form-control:focus {
  z-index: 2;
}

.form-signin input[type="password"] {
  margin-bottom: 10px;
}
</style>
</head>
<body class="bg-mfgreen text-white">
<?php if ($_POST['inputPassword']) {
	if ($settings['admin_password'] ==  md5($_POST['inputPassword'])) {
		$_SESSION['admin_login'] = true;
	}
	else{
	echo '<div class="row justify-content-center"><div class="col-md-4 alert alert-danger" role="alert">Неправильный пароль</div></div>';
	}
}?>

<?php if($_GET['logout'] ){
	unset($_SESSION['admin_login']);
}?>


<?php if (!isset($_SESSION['admin_login'])) {?>
<form class="form-signin text-white" action="./" method="post" role="form">
  <img class="d-block mx-auto mb-4 w-100" src="../img/logo-white.svg" height="72">
  <label for="inputPassword" class="sr-only">Пароль</label>
  <input type="password" id="inputPassword" name="inputPassword" class="form-control" placeholder="Пароль" required="">
  <button class="btn btn-lg text-white btn-mfpurple btn-block" type="submit">Войти</button>

</form>


<?php } else {?>

    <div class="py-5 container">
  <div class="text-center">
    <img class="d-block mx-auto mb-4" src="../img/logo-white.svg" height="72">
  </div>

  <div class="row">
    <div class="col-md-12">
      <h4 class="mb-3">Настройки игры</h4>
      <form id="settings" action="server.php" method="post" role="form">
        <div class="row">
          <div class="col-md-3 mb-3">
            <label for="firstName">Время 1 экрана</label>
            <input type="number" class="form-control" name="time_login" id="time_login" value="<?php echo $settings['time_login']; ?>">
          </div>
          <div class="col-md-3 mb-3">
            <label for="lastName">Время для ответа на экране</label>
            <input type="number" class="form-control" name="time_question_dashboard" id="time_question_dashboard" value="<?php echo $settings['time_question_dashboard']; ?>">
          </div>
		  <div class="col-md-3 mb-3">
            <label for="firstName">Время для ответа в телефоне</label>
            <input type="number" class="form-control" name="time_question_phone" id="time_question_phone" value="<?php echo $settings['time_question_phone']; ?>">
          </div>
          <div class="col-md-3 mb-3">
            <label for="lastName">Количество вопросов</label>
            <input type="number" class="form-control" name="num_questions" id="num_questions" value="<?php echo $settings['num_questions']; ?>">
          </div>
        </div>
        <button class="mt-3 btn btn-primary btn-lg btn-block btn-mfpurple" type="submit">Обновить</button>
      </form>
    </div>
  </div>
  <div class="row my-3">
    <div class="col-md-12 ">
		<h4 class="mb-3">Вопросы</h4>
  <div class="table-responsive">
<table id="questions" class="table bg-white table-hover rounded-bottom">
  <thead>
    <tr>
      <th scope="col">Вопрос</th>
      <th scope="col">Ответ 1</th>
      <th scope="col">Ответ 2</th>
      <th scope="col">Ответ 3</th>
      <th scope="col">Ответ 4</th>
      <th scope="col">Правильный</th>
    </tr>
  </thead>
  <tbody>
	<?php foreach ($questions as $question) { ?>
    <tr data-question="<?php echo $question['id'];?>">
      <td><?php echo $question['question_name'];?></td>
      <td><?php echo $question['answer1'];?></td>
      <td><?php echo $question['answer2'];?></td>
      <td><?php echo $question['answer3'];?></td>
      <td><?php echo $question['answer4'];?></td>
      <td><?php echo $question['answer'];?></td>
    </tr>
	<?php } ?>
  </tbody>
</table>
</div>
<button id="but_add" class="mt-3 btn btn-primary btn-lg btn-block btn-mfpurple">Добавить вопрос</button>
    </div>
  </div> 
 
   <div class="row my-3">
    <div class="col-md-12">
		<h4 class="mb-3">Победители</h4>
		<table class="table bg-white table-hover rounded-bottom">
  <thead>
    <tr>
      <th scope="col">№</th>
      <th scope="col">Номер</th>
      <th scope="col">Баллы</th>
    </tr>
  </thead>
  <tbody>
	<?php $i=1;?>
	<?php if (count($winners)) {?>

	<?php foreach ($winners as $winner) { ?>
    <tr>
      <th scope="row"><?php echo $i; ?></th>
      <td><?php echo $winner['phone'];?></td>
      <td><?php echo $winner['score'];?></td>
    </tr>
	<?php $i++;?>
	<?php } ?>
	<?php } ?>
  </tbody>
</table>
    </div>
  </div> 
  <a href="./?logout=true" class="mt-5 mb-3 text-muted">Выйти</a>

</div>
<script src="../js/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="../js/bootstable.js"></script>
<script>

$('#questions').SetEditable({ 
	$addButton: $('#but_add'),   
	onEdit: function (e){ updateQuestion(e)},
	onAdd: function() { addQuestion()},
	onDelete: function() {},
	onBeforeDelete: function(e) {deleteQuestion(e)},
});

function updateQuestion(e){
	var output = {}
	e.each(function() {
		var tableData = $(this).find('td');
		if (tableData.length > 0) {
			output['ID'] = $(this).context.dataset["question"];
			output['question_name'] = $(tableData[0]).text();
			output['answer1'] = $(tableData[1]).text();
			output['answer2'] = $(tableData[2]).text();
			output['answer3'] = $(tableData[3]).text();
			output['answer4'] = $(tableData[4]).text();
			output['answer'] = $(tableData[5]).text();
		}
	});
	$.ajax({ url: './server.php', type: 'POST', dataType: 'json', data: { question : 'update', data :output}});
}

function addQuestion(){
	$.ajax({
		url: './server.php', type: 'POST', dataType: 'json', 
		data: { question : 'add'},
		complete: function(e) {
			id = e.responseJSON;
			$('#questions').find('tr:last').attr('data-question', id);
		}
	});
}

function deleteQuestion(e){
	var output = {}
	e.each(function() {
		var tableData = $(this).find('td');
		if (tableData.length > 0) {
			output['ID'] = $(this).context.dataset["question"];		}
	});
	$.ajax({url: './server.php', type: 'POST', dataType: 'json', data: { question : 'delete', data :output}});
}

$("#settings").submit(function(event) {
	event.preventDefault();
    var $form = $( this ),
     url = $form.attr( 'action' );      var posting = $.post( url, { time_login: $('#time_login').val(), time_question_dashboard: $('#time_question_dashboard').val(), time_question_phone: $('#time_question_phone').val(), num_questions: $('#num_questions').val() } );

      /* Alerts the results */
      posting.done(function( data ) {
        alert('Настройки обновлены');
      });
    });
</script>
</body>
<?php }?>
</html>
<?php ob_end_flush(); ?>