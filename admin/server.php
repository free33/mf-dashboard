<?php
session_start();
require_once '../config.php';
 
 $db = new Cl_DBclass();
 $output = false;
 
 if(isset( $_POST ) && isset($_SESSION['admin_login']) ){
	$admin_obj = new Cl_Admin();
	 if(isset( $_POST['question'] )){
			if ( $_POST['question'] == 'update') {
			$id = htmlspecialchars($_POST['data']['ID']);
			$question_name = htmlspecialchars($_POST['data']['question_name']);
			$answer1 = htmlspecialchars($_POST['data']['answer1']);
			$answer2 = htmlspecialchars($_POST['data']['answer2']);
			$answer3 = htmlspecialchars($_POST['data']['answer3']);
			$answer4 = htmlspecialchars($_POST['data']['answer4']);
			$answer = htmlspecialchars($_POST['data']['answer']);
			// echo $_POST['data'];
			// echo $id,$question_name,$answer1,$answer2,$answer3,$answer4,$answer;
			echo $admin_obj->updateQuestions($id,$question_name,$answer1,$answer2,$answer3,$answer4,$answer);

			exit;
		} elseif  ( $_POST['question'] == 'add'){
			echo $admin_obj->addQuestions();
		} elseif  ( $_POST['question'] == 'delete'){
			$id = htmlspecialchars($_POST['data']['ID']);
			echo $admin_obj->deleteQuestions($id);
		} 
	}	elseif  ( isset($_POST['time_login']) && isset($_POST['time_question_dashboard']) && isset($_POST['time_question_phone']) && isset($_POST['time_question_dashboard']) && isset($_POST['num_questions'])){
			$time_login = $_POST['time_login'];
			$time_question_dashboard = $_POST['time_question_dashboard'];
			$time_question_phone = $_POST['time_question_phone'];
			$num_questions = $_POST['num_questions'];
			echo $admin_obj->updateOptions($time_login, $time_question_dashboard, $time_question_phone, $num_questions);
		}
}