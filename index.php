<?php 
ob_start();
session_start();
require_once 'config.php'; 

$user_obj = new Cl_Game();
$results = $user_obj->getQuestions();
$settings =$user_obj->getOptions();
?>

<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<title>МегаФон Квиз</title>
	<link href="style.css" rel="stylesheet">
	<script src="js/jquery.min.js"></script>
</head>

<body class="bg-mfgreen">
<div id="screen1" class="x bg-mfgreen hide">
<iframe src="silence.mp3" allow="autoplay" id="audio" style="display:none"></iframe>
<audio id="player" src="mf.mp3" autoplay loop></audio>
<div class="p-top row">
	<a class="logo"><img src="img/logo-white.svg"></a>
	<h1 class="title">Ты сможешь получить приз<br><span  class="color-mfpurple">через <span id="timer1"></span></span></h1>
	<p>
		Хочешь получить клевый подарок?<br>
		Заходи на сайт или сканируй QR-код; отвечай быстрее<br>
		остальных на вопросы и выигрывай!
	</p>
</div>
	<div class="row-bounces">
    <div class="qr"></div>
	<h1 class="link">Скорее заходи<br> на <a>park.promo</a></h1>
	<div class="bounces">
			<div class="bounce bounce-left bg-mfpurple"></div>
			<div class="bounce bounce-middle bg-white"></div>
			<div class="bounce bounce-right bg-mfpurple"></div>
		</div>
	</div>
</div>

<div id="screen2" class="screen bg-mfgreen bg-img1 hide">
	<div class="p-top row-bounces">
		<div class="content">
			<a class="logo as-supersign"><img src="img/logo-white.svg"></a>
			<h1 class="title ">Заходи на park.promo,<br>отвечай и выигрывай!</h1>
		</div>
	</div>
	<div class="row">
		<?php
		$rows =  $results['rowcount'];
		$k = 0; ?>
		<?php foreach ($results['questions'] as $quiz) { ?>
			<div class="hide" id="<?php echo 'question'.$k;?>">
				<h2 class="question-title"><?php echo $quiz['question_name'];?> </h2>
				<div class="question-answer"><?php echo $quiz['answer1'];?></div>
				<div class="question-answer"><?php echo $quiz['answer2'];?></div><br>
				<div class="question-answer"><?php echo $quiz['answer3'];?></div>
				<div class="question-answer"><?php echo $quiz['answer4'];?></div>		
			</div>
			<?php $k++; ?>
		<?php } ?>	
		<p class="question-timer">Успей верно ответить на вопрос за <span id="timer2"></span></p>
	</div>
</div>

<div id="screen3" class="screen bg-mfgreen hide">
	<div class="p-top row-winners">
		<a class="logo as-supersign"><img src="img/logo-white.svg"></a>
		<div class="content">
			<h1 class="title">5 самых быстрых<br>победителей</h1>
			<div class="winners"></div>
		</div>
	</div>
	<div class="row-bottom">
		<h2 class="congrats-text color-mfpurple">Поздравляем!</h2>
		<p class="congrats-text color-mfgreen text-bold">Покажи экран смартфона с победным сообщением <br>и скажи свой номер телефона промоутеру, <br>чтобы получить приятный подарок от МегаФона.</p>
		<p class="congrats-text color-mfpurple text-bold">Хорошего вечера!</p>
	<div class="bounces-winner">
			<div class="bounce bounce-left bg-mfgreen"></div>
			<div class="bounce bounce-middle bg-white"></div>
			<div class="bounce bounce-right bg-mfgreen"></div>
	</div>
	</div>

</div>
	
<script>
var timeLogin = <?php echo $settings['time_login'];?>;
var timePerQuestion = <?php echo $settings['time_question_dashboard'];?>;
var questions = <?php echo $settings['num_questions'];?>;

var count = timeLogin;
var screenNum = 1;
var questionNum = 0;
displayCount(count);
var counter = setInterval(timer, 1000);

$('#screen'+screenNum).removeClass('hide');



function timer() {
	if (count == 0) {
		clearInterval(counter);
		nextStage();
		return;
	}
	count--;
	displayCount(count);
}

function nextStage() {	
	
		if ((screenNum == 1) || (questionNum < (questions-1))){
			if (screenNum == 1) {
			poll('startgame');
			$('#screen'+screenNum).addClass('hide');
			screenNum++;
			$('#screen'+screenNum).removeClass('hide');
			count = timePerQuestion; // продолжительность игры
			displayCount(count);
			counter = setInterval(timer, 1000);
			$('#question'+questionNum).removeClass('hide');	
			return;
			}
			count = timePerQuestion; // продолжительность игры
			displayCount(count);
			counter = setInterval(timer, 1000);
			$('#question'+questionNum).addClass('hide');
			questionNum++;
			$('#question'+questionNum).removeClass('hide');

		}
		else {
			$('#screen'+screenNum).addClass('hide');
			screenNum++;
			$('#screen'+screenNum).removeClass('hide');
			if (screenNum == 3){
				poll('endgame');
				poll('winners');
			}
		}	
}

function displayCount(sec) {
	result = (sec  < 10 ? "0" + sec : sec)  + ((((lasr = sec%100) >= 11 && lasr <= 19) || (lasr = sec%10) >= 5 || lasr == 0) ? ' секунд' :  (lasr == 1 ? ' секунду' : ' секунды'));
	$('#timer'+screenNum).html(result);
}

function poll(message) {
	$.ajax({
		url: './server.php', type: 'GET', dataType: 'json', 
		data: { check : message},
		complete: function(tet) {
			if (message == 'winners') {
				var jsonmessage = tet.responseJSON; 
				$.each(jsonmessage,function(){
					$('.winners').append('<div class="winner"><div class="winner-pic"><img src="'+this.picture+'"></div><div class="winner-phone">'+this.phone+'</div></div>'); 
				});
			}
		return tet.responseJSON;
        }
	})
};

</script>
</body>
</html>
<?php ob_end_flush(); ?>