-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Авг 09 2019 г., 07:27
-- Версия сервера: 5.7.22-22-log
-- Версия PHP: 5.6.37

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `a190275_saltykov`
--

-- --------------------------------------------------------

--
-- Структура таблицы `options`
--

CREATE TABLE `options` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `options`
--

INSERT INTO `options` (`id`, `name`, `value`) VALUES
(1, 'game_session', ''),
(2, 'game_start', '1'),
(3, 'game_end', '0'),
(4, 'questions', '5, 15, 11, 6, 2, 8, 9'),
(5, 'time_login', '5'),
(6, 'time_question_dashboard', '10'),
(9, 'time_question_phone', '15'),
(10, 'num_questions', '7'),
(11, 'admin_password', '6b64298999ef95e6005e71ccd028ff87');

-- --------------------------------------------------------

--
-- Структура таблицы `questions`
--

CREATE TABLE `questions` (
  `id` int(11) NOT NULL,
  `question_name` text NOT NULL,
  `answer1` varchar(250) NOT NULL,
  `answer2` varchar(250) NOT NULL,
  `answer3` varchar(250) NOT NULL,
  `answer4` varchar(250) NOT NULL,
  `answer` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `questions`
--

INSERT INTO `questions` (`id`, `question_name`, `answer1`, `answer2`, `answer3`, `answer4`, `answer`) VALUES
(1, 'В каком году был снят фильм «Унесённые ветром»?', '1959', '1939', '1949', '1929', '2'),
(2, 'Как называется семейная плантация Скарлетт?', 'Торре', 'Тара', 'Тайра', 'Терра', '2'),
(3, 'Как зовут первую любовь Скарлетт?', 'Ретт', 'Брент', 'Эшли', 'Чарльз', '3'),
(5, 'Почему Скарлетт не должна была принимать приглашение Ретта на танец?', 'Она была в трауре', 'Шла война', 'Они не были помолвлены', 'Ретт был женат', '1'),
(6, 'За кого Скарлетт выходит замуж ради сохранения Тары?', 'За брата Эшли', 'За жениха сестры', 'За офицера армии северян', 'За друга Ретта', '2'),
(8, 'Кто становится третьим мужем Скарлетт?', 'Эшли Уилкс', 'Фрэнк Кеннеди', 'Ретт Батлер', 'Стюарт Тарлтон', '3'),
(9, 'О чём Мелани просит Скарлетт на смертном одре?', 'Позаботиться об Эшли', 'Выйти замуж за Эшли', 'Оставить Эшли в покое', 'Уйти от Ретта', '1'),
(11, 'За что отец выгнал Ретта Батлера из дома?', 'За долги', 'За отказ от женитьбы', 'За пристрастие к выпивке', 'За непослушание', '2'),
(12, 'Что происходит с матерью Скартлетт?', 'Её взяли в плен', 'Её выкрали индейцы', 'Она умерла от тифа', 'Она сбежала', '3'),
(13, 'Что предлагает Ретт Скарлетт во время осады Атланты?', 'Сыграть свадьбу', 'Стать его любовницей', 'Пари на победителя в войне', 'Никогда больше не встречаться', '2'),
(14, 'Что сделала Скарлетт, когда к ним пришел солдат-северянин?', 'Прогнала его', 'Приютила у себя', 'Снабдила его всем необходимым', 'Убила его', '4'),
(15, 'Что случилось с Реттом сразу после войны?', 'Его посадили в тюрьму', 'Он уплыл в Англию', 'Он женился на Скарлетт', 'Он вернулся в Тару', '1'),
(16, 'Кто сыграл Скарлетт в фильме?', 'Бетт Дэвис', 'Барбара О` Нил', 'Вивьен Ли', 'Полетт Годдар', '3');

-- --------------------------------------------------------

--
-- Структура таблицы `scores`
--

CREATE TABLE `scores` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `answer` int(11) NOT NULL,
  `right_answer` int(11) NOT NULL,
  `time` float DEFAULT NULL,
  `score` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `phone` text NOT NULL,
  `logged_in` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `phone`, `logged_in`) VALUES
(1, '9995894929', 0),
(2, '79995894929', 1),
(3, '72222222222', 0),
(4, '79514813294', 1),
(5, '9823234091', 1),
(6, '79999999999', 0),
(7, '72333333333', 0),
(8, '79823234091', 1),
(9, '79525141579', 1),
(10, '79658868613', 1),
(11, '79283086206', 1),
(12, '75522562552', 1);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `options`
--
ALTER TABLE `options`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`) USING BTREE;

--
-- Индексы таблицы `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `scores`
--
ALTER TABLE `scores`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`) USING BTREE;

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `phone` (`phone`(20));

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `options`
--
ALTER TABLE `options`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT для таблицы `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT для таблицы `scores`
--
ALTER TABLE `scores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
